package com.ae.checkout;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.ae.model.Apple;
import com.ae.model.Banana;
import com.ae.model.IProduct;

public class ItemScannerTest {

	private IItemScanner scanner;
	
	@Mock
	private ICheckoutService service;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		scanner = new ItemScanner(service);
	}
	

	@SuppressWarnings("unchecked")
	@Test
	public void shouldScan_And_Call_Checkout() {
		scanner.scanItem(mock(IProduct.class));
		verify(service).checkout(anyList());
	}
	
	@Test
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void shouldScan_And_Add_In_Basket() {
		final ArgumentCaptor<List> captor = ArgumentCaptor.forClass(List.class);
		scanner.scanItem(new Apple());
		verify(service).checkout(captor.capture());
		final List<IProduct> items =  captor.getValue();
		assertThat(items.get(0), instanceOf(Apple.class));
	}
	
	@Test
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void shouldScan_And_Add_Items_In_Basket() {
		final ArgumentCaptor<List> captor = ArgumentCaptor.forClass(List.class);
	
		scanner.scanItem(new Apple());
		verify(service).checkout(captor.capture());
		Mockito.reset(service);
		scanner.scanItem(new Banana());
		verify(service).checkout(captor.capture());

		List<IProduct> items =  captor.getValue();
		assertThat(items.get(0), instanceOf(Apple.class));
		assertThat(items.get(1), instanceOf(Banana.class));
	}
}
