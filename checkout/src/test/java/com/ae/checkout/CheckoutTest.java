package com.ae.checkout;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ae.model.Apple;
import com.ae.model.Banana;
import com.ae.model.IProduct;
import com.ae.model.Melon;
import com.ae.model.Orange;

public class CheckoutTest {

	
	private ICheckoutService service;
	
	@Before
	public void setup(){
		service = new CheckoutService();
	}
	
	@Test
	public  void shouldReturn_Total_As_Apple_Price_Given_Basket_Of_One_Apple(){
		List<IProduct> basket = new ArrayList<IProduct>();
		basket.add(new Apple());
		double total = service.checkout(basket);
		Assert.assertEquals(Apple.PRICE, total, 0);
	}
	
	@Test
	public  void shouldReturn_Total_As_Orange_Price_Given_Basket_Of_One_Orange(){
		List<IProduct> basket = new ArrayList<IProduct>();
		basket.add(new Orange());
		double total = service.checkout(basket);
		Assert.assertEquals(Orange.PRICE, total, 0);
	}
	
	@Test
	public  void shouldReturn_Total_Of_Banana_Price_Given_Basket_Of_One_Banana(){
		List<IProduct> basket = new ArrayList<IProduct>();
		basket.add(new Banana());
		double total = service.checkout(basket);
		Assert.assertEquals(Banana.PRICE, total, 0);
	}

	@Test
	public  void shouldReturn_Total_Of_Melon_Price_Given_Basket_Of_One_Melon(){
		List<IProduct> basket = new ArrayList<IProduct>();
		basket.add(new Melon());
		double total = service.checkout(basket);
		Assert.assertEquals(Melon.PRICE, total, 0);
	}
	
	@Test
	public void shouldApply_BuyOneGetOneFree_Offer_For_Apples_In_Basket(){
		List<IProduct> basket = new ArrayList<IProduct>();
		basket.add(new Apple());
		basket.add(new Apple());
		double total = service.checkout(basket);
		Assert.assertEquals(Apple.PRICE, total, 0);
	}

	@Test
	public void shouldApply_3For2_Offer_For_Oranges_In_Basket(){
		List<IProduct> basket = new ArrayList<IProduct>();
		basket.add(new Orange());
		basket.add(new Orange());
		basket.add(new Orange());
		double total = service.checkout(basket);
		Assert.assertEquals(Orange.PRICE * 2, total, 0);
	}
	
	@Test
	public void shouldApply_BuyOneGetOneFree_Offer_For_Bananas_In_Basket(){
		List<IProduct> basket = new ArrayList<IProduct>();
		basket.add(new Banana());
		basket.add(new Banana());
		double total = service.checkout(basket);
		Assert.assertEquals(.20, total, 0);
	}

	@Test
	public void shouldApply_3For2_Offer_For_Melons_In_Basket(){
		List<IProduct> basket = new ArrayList<IProduct>();
		basket.add(new Melon());
		basket.add(new Melon());
		basket.add(new Melon());
		double total = service.checkout(basket);
		Assert.assertEquals(Melon.PRICE * 2, total, 0);
	}

	@Test
	public void shouldApply_3For2_Offer_For_Melons_Separately_To_Oranges(){
		List<IProduct> basket = new ArrayList<IProduct>();
		basket.add(new Melon());
		basket.add(new Melon());
		basket.add(new Orange());
		double total = service.checkout(basket);
		Assert.assertEquals(Melon.PRICE * 2 + Orange.PRICE, total, 0);
	}

	@Test
	public void shouldApply_BuyOneGetOneFree_Offer_For_Bananas_And_Apples_Together_Cheapest_Free(){
		List<IProduct> basket = new ArrayList<IProduct>();
		basket.add(new Apple());
		basket.add(new Apple());
		basket.add(new Apple());
		basket.add(new Banana());
		basket.add(new Banana());
		double total = service.checkout(basket);
		Assert.assertEquals((Apple.PRICE * 3), total, 0);
	}

	@Test
	public  void shouldAdd_Price_Of_All_Items_Along_With_Their_Offers_To_Calculate_Total(){
		List<IProduct> basket = new ArrayList<IProduct>();
		
		basket.add(new Apple());
		basket.add(new Apple());
		
		basket.add(new Banana());
		basket.add(new Banana());
		basket.add(new Banana());
		
		basket.add(new Orange());
		basket.add(new Orange());
		basket.add(new Orange());
		basket.add(new Orange());
		
		basket.add(new Melon());
		basket.add(new Melon());
		
		double total = service.checkout(basket);
		Assert.assertEquals((Apple.PRICE * 2) + (Banana.PRICE) + (Orange.PRICE *3) + (Melon.PRICE * 2), total, 0);
	}
	
	@Test
	public void shouldReturn_ZERO_Given_An_Empty_List(){
		List<IProduct> basket = new ArrayList<IProduct>();
		double total = service.checkout(basket);
		Assert.assertEquals(0, total, 0);
	}
	
	@Test
	public void shouldReturn_ZERO_Given_Null_List(){
		double total = service.checkout(null);
		Assert.assertEquals(0, total, 0);
	}
	
}
