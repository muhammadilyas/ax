import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.ae.checkout.CheckoutTest;
import com.ae.checkout.ItemScannerTest;

@RunWith(Suite.class)
@SuiteClasses({
	CheckoutTest.class,
	ItemScannerTest.class
})
public class CheckoutTestSuite {

}
