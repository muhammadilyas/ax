package com.ae.model;


public class Orange implements IProduct {

	public static final double PRICE = 0.25;

	public double getPrice() {
		return PRICE;
	}
	
	@Override
	public IOffer getOffer(){
		return IProduct.threeForTwo();
	}

}
