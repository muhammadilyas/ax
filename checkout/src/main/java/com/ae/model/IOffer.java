package com.ae.model;

import java.util.List;

public interface IOffer {
	
	String getName();
	
	ISumCalculator getSumCalculator();
	
	interface ISumCalculator{
		double calculateSum(List<IProduct> items);
	}
}
