package com.ae.model;

public class Apple implements IProduct {

	public static final double PRICE = 0.60;
	
	public double getPrice() {
		return PRICE;
	}

	@Override
	public IOffer getOffer(){
		return IProduct.buyOneGetCheapestOneFree();
	}
	
}
