package com.ae.model;

public class Banana implements IProduct {

	public static final double PRICE = 0.20;

	public double getPrice() {
		return PRICE;
	}

	@Override
	public IOffer getOffer(){
		return IProduct.buyOneGetCheapestOneFree();
	}
	
}
