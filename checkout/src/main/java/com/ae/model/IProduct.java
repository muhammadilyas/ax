package com.ae.model;

public interface IProduct {

	double getPrice();
	
	public default IOffer getOffer(){
		return new NoOffer();
	}

	public static IOffer buyOneGetCheapestOneFree() {
		return new BuyOneGetCheapestOneFree();
	}

	public static IOffer threeForTwo() {
		return new ThreeForTwo();
	}

	
	class ThreeForTwo extends Offer {
		
		@Override
		public String getName() {
			return "ThreeForTwo";
		}

		@Override
		public ISumCalculator getSumCalculator() {
			return (list) -> list.stream()
					 .limit(((list.size()/3)*2) + (list.size()%3))
					 .mapToDouble(p->p.getPrice())
					 .sum();
		}
	}

	class BuyOneGetCheapestOneFree extends Offer {
		
		@Override
		public String getName() {
			return "BuyOneGetCheapestOneFree";
		}

		@Override
		public ISumCalculator getSumCalculator() {
			return (list) -> list.stream()
					.sorted((item1, item2) -> Double.valueOf(item2.getPrice()).compareTo(Double.valueOf(item1.getPrice())))
					.limit((list.size() / 2) + (list.size() % 2))
					.mapToDouble(p->p.getPrice())
					.sum();
		}
	}

	class NoOffer extends Offer {
		
		@Override
		public String getName() {
			return "NO_OFFER";
		}

		@Override
		public ISumCalculator getSumCalculator() {
			return (list) -> list.stream().mapToDouble(p->p.getPrice()).sum();
		}
	}
	
	abstract class Offer implements IOffer{

		@Override
		public String toString() {
			return getName();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Offer other = (Offer) obj;
			if (getName() == null) {
				if (other.getName() != null)
					return false;
			} else if (!getName().equals(other.getName()))
				return false;
			return true;
		}
		
	}
	
}




