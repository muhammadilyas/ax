package com.ae.model;

public class Melon implements IProduct {

	public static final double PRICE = 1.0;
	
	public double getPrice() {
		return PRICE;
	}

	@Override
	public IOffer getOffer(){
		return new ThreeForTwo() {
			@Override
			public String getName() {
				return "ThreeForTwoForMelon";
			}
		};
	}
	
}
