package com.ae.checkout;

import java.util.List;

import com.ae.model.IProduct;

public interface ICheckoutService {
	double checkout(List<IProduct> basket);
}
