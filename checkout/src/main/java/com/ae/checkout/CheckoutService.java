package com.ae.checkout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ae.model.IOffer;
import com.ae.model.IProduct;

public class CheckoutService implements ICheckoutService {

	public double checkout(List<IProduct> basket) {
		
		if(basket == null)
			return 0;

		Map<IOffer, List<IProduct>> itemsByOffer = basket.stream().collect(Collectors.groupingBy(p->p.getOffer()));
		Map<IOffer,Double> totalByOffer = new HashMap<>();
		
		itemsByOffer.forEach((offer, list) -> {
			totalByOffer.put(offer, offer.getSumCalculator().calculateSum(list));
		});
	
		return totalByOffer.values().stream().mapToDouble(d->d).sum();
			
	}
	
}
