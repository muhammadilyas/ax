package com.ae.checkout;

import com.ae.model.IProduct;

public interface IItemScanner {
	double scanItem(IProduct product);
}
