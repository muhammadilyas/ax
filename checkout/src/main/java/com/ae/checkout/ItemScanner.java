package com.ae.checkout;

import java.util.ArrayList;
import java.util.List;

import com.ae.model.IProduct;

public class ItemScanner implements IItemScanner{

	private List<IProduct> basket = new ArrayList<>();
	
	private ICheckoutService checkoutService;
	
	public ItemScanner(ICheckoutService checkoutService) {
		this.checkoutService = checkoutService;
	}

	@Override
	public double scanItem(IProduct product) {
		basket.add(product);
		return checkoutService.checkout(basket);
	}
	
}
